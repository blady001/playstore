/**
  * Created by macblady on 08.05.2017.
  */
import javax.inject.Inject

import play.api.http.HttpFilters
import play.filters.cors.CORSFilter


class Filters @Inject() (corsFilter: CORSFilter) extends HttpFilters {
  def filters = Seq(corsFilter)
}
