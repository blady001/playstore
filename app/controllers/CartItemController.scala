package controllers

import javax.inject.Inject

import dao.{CartItemDAO, OrderDAO}
import models.{AddCartItemREST, CartItem, CartItemsREST, Order}
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by macblady on 23.04.2017.
  */
class CartItemController @Inject() (cartItemDAO: CartItemDAO, orderDAO: OrderDAO) extends Controller {

  def list = Action.async { implicit request =>
    cartItemDAO.all map {
      cartItems => Ok(Json.toJson(cartItems))
    }
  }

  def create = Action { implicit request =>
    var jsondata: AddCartItemREST = request.body.asJson.get.as[AddCartItemREST]

    var orderId = -1

    val activeOrder = Await.result(orderDAO.getActiveForUser(jsondata.userId), Duration.Inf)
    if (activeOrder.isEmpty) {
      var newOrder = Order(id=0, userId=jsondata.userId, totalAmount = "", isRealized = false, paymentMethod="", shippingAddress = "")
      orderId = Await.result(orderDAO.insert(newOrder), Duration.Inf)
    }
    else {
      orderId = activeOrder.get.id
    }

    var cartItem = CartItem(id=0, productId=jsondata.productId, orderId=orderId, quantity=1)
    cartItemDAO.insert(cartItem)

    Ok(Json.toJson("{success: true}"))
  }

  def delete(cartItemId: Int) = Action { implicit request =>
    cartItemDAO.delete(cartItemId)
    Ok(Json.toJson("{success: true}"))
  }

  def update(cartItemId: Int) = Action { implicit request =>
    var jsondata: CartItemsREST = request.body.asJson.get.as[CartItemsREST]
    var itemToUpdate = CartItem(id=jsondata.id, productId=jsondata.productId, orderId=jsondata.orderId, quantity=jsondata.quantity)
    cartItemDAO.update(id=jsondata.id, itemToUpdate)
    Ok(Json.toJson("{success: true}"))
  }

  def getForUser(userId: Int) = Action { implicit request =>
    val activeOrder = Await.result(orderDAO.getActiveForUser(userId), Duration.Inf)
    if (!activeOrder.isEmpty) {
      val cartItems = Await.result(cartItemDAO.getByOrderId(activeOrder.get.id), Duration.Inf)
      Ok(Json.toJson(cartItems))
    }
    else {
      Ok(Json.arr())
    }

//    cartItems map {
//      cartItems => Ok(Json.toJson(cartItems))
//    }
//    val future = orderDAO.getActiveForUser(userId)
//    future.onSuccess(
//      case Some(activeOrder) => {
//        cartItemDAO.getByOrderId(activeOrder.get.id) map {
//          cartItems => Ok(Json.toJson(cartItems))
//        }
//      }
//      case None => Ok(Json.arr())
//    )


//      Ok(Json.arr())
//    orderDAO.getActiveForUser(userId).map {
//      case Some(activeOrder) => {
//        cartItemDAO.getByOrderId(activeOrder.id) map {
//          cartItems => Ok(Json.toJson(cartItems))
//        }
//      }
//      case None =>
//        Ok(Json.arr())
//    }
//    cartItemDAO.getByOrderId(activeOrder.id) map {
//      cartItems => Ok(Json.toJson(cartItems))
//    }

//    find(loginInfo).map {
//      case Some(authInfo) =>
//        update(loginInfo, authInfo)
//        Ok("Updated AuthInfo")               //Return a play.api.mvc.Resul
//      case None =>
//        add(loginInfo, authInfo)
//        Created("CreatedAuthInfo")           //Return another play.api.mvc.Resul
//    }
//    }
//    else {
//    Ok(Json.arr())
//    }
//    else {
//      Ok(Json.toJson("[]"))
//    }
  }

}
