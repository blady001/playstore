package controllers

import javax.inject.Inject

import dao.{CategoryDAO, ProductDAO}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by macblady on 17.05.2017.
  */
class CategoryController @Inject() (categoryDAO: CategoryDAO, productDAO: ProductDAO) extends Controller {

  def list = Action.async { implicit request =>
    categoryDAO.all map {
      categories => Ok(Json.toJson(categories))
    }
  }

  def products(categoryId: Int) = Action.async { implicit request =>
    productDAO.getByCategoryId(categoryId) map {
      products => Ok(Json.toJson(products))
    }
  }

}
