package controllers

import javax.inject.Inject

import dao.OrderDAO
import models.{Order, OrdersREST}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by macblady on 23.04.2017.
  */
class OrderController @Inject() (orderDAO: OrderDAO) extends Controller {

  def list = Action.async { implicit request =>
    orderDAO.all map {
      orders => Ok(Json.toJson(orders))
    }
  }

  def get(orderId: Int) = Action.async { implicit request =>
    orderDAO.getById(orderId) map {
      order => Ok(Json.toJson(order))
    }
  }

  def getActiveForUser(userId: Int) = Action.async { implicit request =>
    orderDAO.getByUserId(userId, false) map {
      order => Ok(Json.toJson(order))
    }
  }

  def update(orderId: Int) = Action { implicit request =>
    var jsondata: OrdersREST = request.body.asJson.get.as[OrdersREST]

    var orderToUpdate = Order(id=jsondata.id, userId=jsondata.userId, totalAmount = jsondata.totalAmount, isRealized = jsondata.isRealized, paymentMethod = jsondata.paymentMethod, shippingAddress = jsondata.shippingAddress)
    orderDAO.update(jsondata.id, orderToUpdate)
    Ok(Json.toJson("{success: true}"))
  }
}
