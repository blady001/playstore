package controllers

import javax.inject.Inject

import dao.{CartItemDAO, OrderDAO, ProductDAO}
import models._
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext


/**
  * Created by macblady on 23.04.2017.
  */
class ProductController @Inject() (productDAO: ProductDAO, orderDAO: OrderDAO, cartItemDAO: CartItemDAO) extends Controller {

  def list = Action.async { implicit request =>
    productDAO.all map {
      products => Ok(Json.toJson(products))
    }
  }

  def create = Action { implicit request =>
    var jsondata: ProductsREST = request.body.asJson.get.as[ProductsREST]
    var product = Product(id=0, name=jsondata.name, description=jsondata.description, price=jsondata.price, amount=jsondata.amount, categoryId=jsondata.categoryId, imageUrl=jsondata.imageUrl)
    productDAO.insert(product)
    Ok(request.body.asJson.get)
  }

  def get(productId: Int) = Action async { implicit request =>
    productDAO.getById(productId) map {
      product => Ok(Json.toJson(product))
    }
  }

//  def addToCart(productId: Int) = Action { implicit request =>
//    var jsondata: UserCredentialsREST = request.body.asJson.get.as[UserCredentialsREST]
//
//    var orderId = -1
//
//    val activeOrder = Await.result(orderDAO.getActiveForUser(jsondata.userId), Duration.Inf)
//    if (activeOrder.isEmpty) {
//      var newOrder = Order(id=0, userId=jsondata.userId, totalAmount = "")
//      orderId = Await.result(orderDAO.insert(newOrder), Duration.Inf)
//    }
//    else {
//      orderId = activeOrder.get.id
//    }
//
//    var cartItem = CartItem(id=0, productId=productId, orderId=orderId, quantity=1)
//    cartItemDAO.insert(cartItem)
//
//    Ok(Json.toJson("{success:true}"))
//
//  }

//  def newproduct = Action { implicit request =>
//    var json:ProductsREST = request.body.asJson.get.as[ProductsREST]
//    var product = Products(prodId = 0, tytul = json.tytul, opis = json.opis)
//    productsDAO.insert(product)
//    Ok(request.body.asJson.get)
//  }

}
