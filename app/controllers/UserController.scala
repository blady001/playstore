package controllers

import javax.inject.Inject

import dao.UserDAO
import models.{User, UserREST}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by macblady on 17.05.2017.
  */
class UserController @Inject()(userDAO: UserDAO) extends Controller {

  def list = Action.async { implicit request =>
    userDAO.all map {
      users => Ok(Json.toJson(users))
    }
  }

  def get(userId: Int) = Action.async { implicit request =>
    userDAO.getById(userId) map {
      user => Ok(Json.toJson(user))
    }
  }

  def update(userId: Int) = Action { implicit request =>
    var jsondata: UserREST = request.body.asJson.get.as[UserREST]
//    val user = userDAO.getUser(jsondata.userId)

    var userToUpdate = User(id=jsondata.userId, username=jsondata.username, password=jsondata.password, firstname=jsondata.firstname, lastname=jsondata.lastname, email=jsondata.email, address=jsondata.address)
    userDAO.update(jsondata.userId, userToUpdate)
    Ok(Json.toJson("{success: true}"))
  }

}
