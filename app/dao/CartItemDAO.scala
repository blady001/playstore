package dao

import javax.inject.Inject

import models.{CartItem, CartItemsREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by macblady on 23.04.2017.
  */
class CartItemDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  private val CartItems = TableQuery[CartItemsTable]

  def all(implicit ec: ExecutionContext): Future[List[CartItemsREST]] = {
    val query = CartItems
    val results = query.result
    val futureCartItems = db.run(results)
    futureCartItems.map(
      _.map {
        a => CartItemsREST(id = a.id, productId = a.productId, orderId = a.orderId, quantity = a.quantity)
      }.toList
    )
  }

  def getByOrderId(orderId: Int): Future[List[CartItemsREST]] = {
    val query = CartItems
    val result = query.filter(_.orderId === orderId).result
    val futureCartItems = db.run(result)

    futureCartItems.map(
      _.map {
        a => CartItemsREST(id = a.id, productId = a.productId, orderId = a.orderId, quantity = a.quantity)
      }.toList
    )
  }

  def insert(cartItem: CartItem): Future[Unit] = db.run(CartItems += cartItem).map { _ => () }
//
  def delete(id: Int): Future[Unit] = db.run(CartItems.filter(_.id === id).delete).map(_ => ())
//
  def update(id: Int, cartItem: CartItem): Future[Unit] = {
    val cartItemsToUpdate: CartItem = cartItem.copy(id)
    db.run(CartItems.filter(_.id === id).update(cartItemsToUpdate)).map(_ => ())
  }

  private class CartItemsTable(tag: Tag) extends Table[CartItem](tag, "CART_ITEM") {

    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)

    def productId = column[Int]("PRODUCT_ID")

    def orderId = column[Int]("ORDER_ID")

    def quantity = column[Int]("QUANTITY")

    def * = (id, productId, orderId, quantity) <>(CartItem.tupled, CartItem.unapply _)

    // TODO: How to define foreign key?

  }

}
