package dao

import javax.inject.Inject

import models.{Category, CategoryREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by macblady on 17.05.2017.
  */
class CategoryDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]{

  import driver.api._

  private val Categories = TableQuery[CategoriesTable]

  def all(implicit ec: ExecutionContext): Future[List[CategoryREST]] = {
    val query = Categories
    val results = query.result
    val futureCategories = db.run(results)
    futureCategories.map(
      _.map {
        a => CategoryREST(id = a.id, name = a.name, description = a.description)
      }.toList
    )
  }

  private class CategoriesTable(tag: Tag) extends Table[Category](tag, "TAB_CATEGORY") {

    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)

    def name = column[String]("NAME")

    def description = column[String]("DESCRIPTION")

    def * = (id, name, description) <>(Category.tupled, Category.unapply _)
  }

}
