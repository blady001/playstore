package dao

import javax.inject.Inject

import models.{Order, OrdersREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by macblady on 23.04.2017.
  */
class OrderDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  private val Orders = TableQuery[OrdersTable]

  def all(implicit ec: ExecutionContext): Future[List[OrdersREST]] = {
    val query = Orders
    val results = query.result
    val futureOrders = db.run(results)
    futureOrders.map(
      _.map {
        a => OrdersREST(id = a.id, userId = a.userId, totalAmount = a.totalAmount, isRealized = a.isRealized, paymentMethod = a.paymentMethod,
          shippingAddress = a.shippingAddress)
      }.toList
    )
  }

  def doesActiveOrderExistForUser(userId: Int): Future[Boolean] = {
    val query = Orders
    db.run(query.filter(_.userId === userId).exists.result)
  }

  def getActiveForUser(userId: Int): Future[Option[Order]] = {
    val query = Orders
    db.run(query.filter(_.userId === userId).filter(_.isRealized === false).result.headOption)
  }

  def getById(id: Int): Future[OrdersREST] = {
    val query = Orders
    val result = query.filter(_.id === id).result.headOption
    val futureOrder = db.run(result)

    futureOrder.map(
      _.map {
        a => OrdersREST(id = a.id, userId = a.userId, totalAmount = a.totalAmount, isRealized = a.isRealized, paymentMethod = a.paymentMethod,
          shippingAddress = a.shippingAddress)
      }.toList.head
    )
  }

  def getByUserId(userId: Int, isRealized: Boolean): Future[OrdersREST] = {
    val query = Orders
    val result = query.filter(_.userId === userId).filter(_.isRealized === isRealized).result.headOption
    val futureOrder = db.run(result)

    futureOrder.map(
      _.map {
        a => OrdersREST(id = a.id, userId = a.userId, totalAmount = a.totalAmount, isRealized = a.isRealized, paymentMethod = a.paymentMethod,
          shippingAddress = a.shippingAddress)
      }.toList.head
    )
  }

//  val insertQuery = Orders returning Orders.map(_.id) into ((item, id) => item.copy(id = id))

  def insert(order: Order): Future[Int] = {
    db.run(Orders returning Orders.map(_.id) += order)
  }
//
//  def delete(id: Int): Future[Unit] = db.run(Orders.filter(_.id === id).delete).map(_ => ())
//
  def update(id: Int, order: Order): Future[Unit] = {
    val ordersToUpdate: Order = order.copy(id)
    db.run(Orders.filter(_.id === id).update(ordersToUpdate)).map(_ => ())
  }

  private class OrdersTable(tag: Tag) extends Table[Order](tag, "TAB_ORDER") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def userId = column[Int]("USER_ID")
    def totalAmount = column[String]("TOTAL_AMOUNT")
    def isRealized = column[Boolean]("IS_REALIZED")
    def paymentMethod = column[String]("PAYMENT_METHOD")
    def shippingAddress = column[String]("SHIPPING_ADDRESS")
    def * = (id, userId, totalAmount, isRealized, paymentMethod, shippingAddress) <>(Order.tupled, Order.unapply _)
    // TODO: Fk to user?
  }

}
