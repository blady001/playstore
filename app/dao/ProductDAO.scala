package dao

import javax.inject.Inject

import models.{Product, ProductsREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by macblady on 23.04.2017.
  */
class ProductDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  private val Products = TableQuery[ProductsTable]

  def all(implicit ec: ExecutionContext): Future[List[ProductsREST]] = {
    val query = Products
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
        a => ProductsREST(id = a.id, name = a.name, description = a.description, price = a.price, amount = a.amount, categoryId = a.categoryId, imageUrl = a.imageUrl)
      }.toList
    )
  }

  def insert(product: Product): Future[Unit] = db.run(Products += product).map { _ => () }

  def getById(id: Int): Future[ProductsREST] = {
    val query = Products
    val result = query.filter(_.id === id).result.headOption
    val futureProduct = db.run(result)

    futureProduct.map(
      _.map {
        a => ProductsREST(id = a.id, name = a.name, description = a.description, price = a.price, amount = a.amount, categoryId = a.categoryId, imageUrl = a.imageUrl)
      }.toList.head
    )
  }

  def getByCategoryId(categoryId: Int): Future[List[ProductsREST]] = {
    val query = Products
    val result = query.filter(_.categoryId === categoryId).result
    val futureProducts = db.run(result)

    futureProducts.map(
      _.map {
        a => ProductsREST(id = a.id, name = a.name, description = a.description, price = a.price, amount = a.amount, categoryId = a.categoryId, imageUrl = a.imageUrl)
      }.toList
    )
  }

//  def insert(product: Product): Future[Unit] = db.run(Products += product).map { _ => () }
//
//  def delete(name: String): Future[Unit] = db.run(Products.filter(_.name === name).delete).map(_ => ())

//  def update(id: Int, product: Product): Future[Unit] = {
//    val productToUpdate: Product = product.copy(id)
//    db.run(Products.filter(_.id === id).update(productToUpdate)).map(_ => ())
//  }

  private class ProductsTable(tag: Tag) extends Table[Product](tag, "PRODUCT") {

    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)

    def name = column[String]("NAME")

    def description = column[String]("DESCRIPTION")

    def price = column[String]("PRICE")

    def amount = column[Int]("AMOUNT")

    def categoryId = column[Int]("CATEGORY_ID")

    def imageUrl = column[String]("IMAGE_URL")

    def * = (id, name, description, price, amount, categoryId, imageUrl) <>(Product.tupled, Product.unapply _)
  }

}
