package dao

import javax.inject.Inject

import models.{User, UserREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by macblady on 23.04.2017.
  */
class UserDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  private val Users = TableQuery[UsersTable]

  def all(implicit ec: ExecutionContext): Future[List[UserREST]] = {
    val query = Users
    val results = query.result
    val futureUsers = db.run(results)
    futureUsers.map(
      _.map {
        a => UserREST(userId = a.id, username = a.username, password = a.password, firstname = a.firstname,
          lastname = a.lastname, email = a.email, address = a.address)
      }.toList
    )
  }

  def insert(user: User): Future[Unit] = db.run(Users += user).map { _ => () }

  def delete(id: Int): Future[Unit] = db.run(Users.filter(_.id === id).delete).map(_ => ())

  def update(id: Int, user: User): Future[Unit] = {
    val userToUpdate: User = user.copy(id)
    db.run(Users.filter(_.id === id).update(userToUpdate)).map(_ => ())
  }

  def getById(id: Int): Future[UserREST] = {
    val query = Users
    val result = query.filter(_.id === id).result.headOption
    val futureUser = db.run(result)

    // TODO: Add some try/catch?
    futureUser.map(
      _.map {
        a => UserREST(userId = a.id, username = a.username, password = a.password, firstname = a.firstname,
          lastname = a.lastname, email = a.email, address = a.address)
      }.toList.head
    )
  }

  def getUser(id: Int): Future[Option[User]] = {
    val query = Users
    db.run(query.filter(_.id === id).result.headOption)
  }

  private class UsersTable(tag: Tag) extends Table[User](tag, "USER") {

    def id = column[Int]("ID", O.PrimaryKey)

    def username = column[String]("USERNAME")

    def password = column[String]("PASSWORD")

    def firstname = column[String]("FIRSTNAME")

    def lastname = column[String]("LASTNAME")

    def email = column[String]("EMAIL")

    def address = column[String]("ADDRESS")

    def * = (id, username, password, firstname, lastname, email, address) <>(User.tupled, User.unapply _)


  }

}
