package models

/**
  * Created by macblady on 10.04.2017.
  */

case class Product(
                    id: Int,
                    name: String,
                    description: String,
                    price: String,
                    amount: Int,
                    categoryId: Int,
                    imageUrl: String
                  )

case class User(
                 id: Int,
                 username: String,
                 password: String,
                 firstname: String,
                 lastname: String,
                 email: String,
                 address: String
               )

case class Order(id: Int, userId: Int, totalAmount: String, isRealized: Boolean, paymentMethod: String, shippingAddress: String)

case class CartItem(id: Int, productId: Int, orderId: Int, quantity: Int)

case class Category(id: Int, name: String, description: String)

