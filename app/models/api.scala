package models

import play.api.libs.json.Json

/**
  * Created by macblady on 24.04.2017.
  */

case class ProductsREST(id: Int, name: String, description: String, price: String, amount: Int, categoryId: Int, imageUrl: String)

object ProductsREST {
  implicit val productsFormat = Json.format[ProductsREST]
}


case class OrdersREST(id: Int, userId: Int, totalAmount: String, isRealized: Boolean, paymentMethod: String, shippingAddress: String)

object OrdersREST {
  implicit val ordersFormat = Json.format[OrdersREST]
}

case class AddCartItemREST(userId: Int, username: String, password: String, productId: Int)

object AddCartItemREST {
  implicit val addCartItemFormat = Json.format[AddCartItemREST]
}

case class CartItemsREST(id: Int, productId: Int, orderId: Int, quantity: Int)

object CartItemsREST {
  implicit val cartItemsFormat = Json.format[CartItemsREST]
}

case class UserREST(userId: Int, username: String, password: String, firstname: String, lastname: String, email: String, address: String)

object UserREST {
  implicit val userFormat = Json.format[UserREST]
}

case class UserCredentialsREST(userId: Int, username: String, password: String)

object UserCredentialsREST {
  implicit val userCredentialsFormat = Json.format[UserCredentialsREST]
}

case class CategoryREST(id: Int, name: String, description: String)

object CategoryREST {
  implicit val categoryFormat = Json.format[CategoryREST]
}
