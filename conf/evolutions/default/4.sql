# --- !Ups
ALTER TABLE TAB_ORDER ADD COLUMN PAYMENT_METHOD TEXT;
ALTER TABLE TAB_ORDER ADD COLUMN SHIPPING_ADDRESS TEXT;

# --- !Downs
ALTER TABLE TAB_ORDER DROP COLUMN IF EXISTS PAYMENT_METHOD;
ALTER TABLE TAB_ORDER DROP COLUMN IF EXISTS SHIPPING_ADDRESS;
