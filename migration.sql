CREATE TABLE TAB_CATEGORY (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    NAME TEXT NOT NULL,
    DESCRIPTION TEXT
);

CREATE TABLE PRODUCT (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    NAME TEXT NOT NULL,
    DESCRIPTION TEXT,
    PRICE TEXT NOT NULL,
    AMOUNT INTEGER NOT NULL,
    IMAGE_URL TEXT,
    CATEGORY_ID INTEGER,
    FOREIGN KEY(CATEGORY_ID) REFERENCES TAB_CATEGORY(ID)
);

CREATE TABLE USER (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    USERNAME TEXT NOT NULL,
    PASSWORD TEXT NOT NULL,
    FIRSTNAME TEXT NOT NULL,
    LASTNAME TEXT NOT NULL,
    EMAIL TEXT NOT NULL,
    ADDRESS TEXT
);

CREATE TABLE TAB_ORDER (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    USER_ID INTEGER,
    TOTAL_AMOUNT TEXT,
    IS_REALIZED BOOLEAN DEFAULT FALSE,
    PAYMENT_METHOD TEXT,
    SHIPPING_ADDRESS TEXT,
    FOREIGN KEY(USER_ID) REFERENCES USER(ID)
);

CREATE TABLE CART_ITEM (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    PRODUCT_ID INTEGER,
    ORDER_ID INTEGER,
    QUANTITY INTEGER,
    FOREIGN KEY(PRODUCT_ID) REFERENCES PRODUCT(ID),
    FOREIGN KEY(ORDER_ID) REFERENCES TAB_ORDER(ID)
);

INSERT INTO TAB_CATEGORY (NAME, DESCRIPTION) VALUES ('Longboard', 'A longboard is a type of sports equipment similar, but not the same as a skateboard. It is much longer than a skateboard. Often faster because of wheel size, longboards are commonly used for cruising, downhill racing, slalom racing, sliding, dancing, long distance racing, and transport.');
INSERT INTO TAB_CATEGORY (NAME, DESCRIPTION) VALUES ('Skateboard', 'A skateboard is a type of sports equipment or toy used primarily for the activity of skateboarding. It usually consists of a specially designed maplewood board combined with a polyurethane coating used for making smoother slides and stronger durability. Most skateboards are made with 7 plies of this wood.');
INSERT INTO TAB_CATEGORY (NAME, DESCRIPTION) VALUES ('Cruiser', 'A cruiser is more a board for going through town, it will have wide, loose trucks that are great for carving and a kick tail on the back for stopping or giving the board pop over a small crack.');

INSERT INTO PRODUCT (NAME, DESCRIPTION, PRICE, AMOUNT, IMAGE_URL, CATEGORY_ID) VALUES ('Landyachtz Osteon', 'Perfect for DH', '129.99', 2, 'http://i.ebayimg.com/00/s/MTIwMFgxNjAw/z/BV4AAOSw3ydVlehS/$_1.JPG', 1);
INSERT INTO PRODUCT (NAME, DESCRIPTION, PRICE, AMOUNT, IMAGE_URL, CATEGORY_ID) VALUES ('Real 8', 'Cheap & funny', '99.00', 1, 'https://recognizerj5.files.wordpress.com/2013/08/real_deck_1.jpg', 2);

INSERT INTO USER (USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, ADDRESS) VALUES ('admin', 'admin', 'Admin', 'Adminowicz', 'admin@play.sucks', 'Some address 13/24');

